﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
		parent::__construct();
		// Load url helper
		
		$this->load->model('questions_model');
		$this->load->model('answers_model');

	}

	

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index 
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		$res = array('message' => '');
		
		if (!empty($_POST) && isset($_POST['submit'])) { 
				print_r($_POST);
			
			$questionCount = $this->input->post('count_question');
			// here is code to receive the form values
			for($i=1; $i<=$questionCount;$i++){
				$question = $this->input->post('question_'.$i);
				$data['que_desc'] = $question ;
		 		$data['parent_que_id'] = '0' ;
				$id = $this->questions_model->insert_entry($data);
				if($id) {
					$temp = $this->input->post('questionsel_'.$i) ;	
					if($temp == 'option_'.$i) {
						for($k=1; $k<=5;$k++) {
							$multi_answer = $this->input->post('main_answer_'.$i.'_'.$k);
							$multi_ans_data['ans_desc'] = $multi_answer ;
				 			$multi_ans_data['parent_que_id'] = $id ;
							$ans_id = $this->answers_model->insert_entry($multi_ans_data);
						}
					}
					else {
						$answer = $this->input->post('main_answer_'.$i);
						$ans_data['ans_desc'] = $answer ;
				 		$ans_data['parent_que_id'] = $id ;
						$ans_id = $this->answers_model->insert_entry($ans_data);
					}
				}
					
				$subQuesCount = $this->input->post('sub_question_'.$i.'_count');
				for($j=1; $j<=$subQuesCount;$j++){
					
					$subQuestion = $this->input->post('sub_question_'.$i.'_'.$j);
					$sub_que_data['que_desc'] = $subQuestion ;
						$sub_que_data['parent_que_id'] = $id ;
					$sub_id = $this->questions_model->insert_entry($sub_que_data);
					if($sub_id){

						$sub_que_temp = $this->input->post('sub_questionsel_'.$i."_".$j) ;	
						if($sub_que_temp == 'option_'.$i."_".$j) {
							for($k=1; $k<=5;$k++) {
								$multi_answer = $this->input->post('sub_answer_'.$i.'_'.$j."_".$k);
								$multi_ans_data['ans_desc'] = $multi_answer ;
					 			$multi_ans_data['parent_que_id'] = $sub_id ;
								$ans_id = $this->answers_model->insert_entry($multi_ans_data);
							}
						} 
						else {

							$subAnswer = $this->input->post('sub_answer_'.$i.'_'.$j);
							$sub_ans_data['ans_desc'] = $subAnswer ;
		 					$sub_ans_data['parent_que_id'] = $sub_id ;
							$ans_id = $this->answers_model->insert_entry($sub_ans_data);
						}
					}
				}
			}

			$res = array(
        		'message' => 'Data submitted Succcessfully'
			);

		}
		$this->load->view('welcome',$res);
	}

	public function home() {
		$this->load->view('welcome');
	}
}
