<?php
class Questions_model extends CI_Model {

        public function __construct()
        {
                parent::__construct();
                // Your own constructor code
        }
	
	
	public function get_data($slug = FALSE)
	{
		if ($slug === FALSE)
		{
		        $query = $this->db->get('questions');
		        return $query->result_array();
		}

		$query = $this->db->get_where('questions', array('slug' => $slug));
		return $query->row_array();
	}
}

