<html>
<head>
<title>Advanz101 Assignment</title>

<link  rel="stylesheet" type="text/css"   href="<?php echo base_url('assets/css/basic.css'); ?>" >

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- JS FILES -->

<script type='text/javascript' src="<?php echo base_url('assets/js/jquery-3.3.1.min.js'); ?>"></script>

</head>
<body>

<div class="main">
    <form name="registration" method="post" >
    <input type="hidden" value="0" id="count_question" name="count_question"> 
    
    <input type="hidden" value="0" id="count_sub_question" name="count_sub_question" > 
    <h4 class="success"><?php echo $message ; ?></h4> 
    <div class="head">
      ADD NEW CALL
    </div>
    <div class="container">
      <table id="question_table">
        <tr>
          <td><span id="error" class="error">* Please fill required fields.</span></td>
        </tr>
      </table>
      
    </div>
    <div class="footer">
        <div class="question_block">
          <a id="addquestion" class="addquestion">+ Add New Question</a>
        </div>
        <div class="button_block">
          <button type="submit" class="save" name="submit" id="save">SAVE</button>
          <button class="cancel" id="cancel" >Cancel</button>
        </div>
    </div>
    </form>
</div>
</body>
</html>
<script>
  $(document).ready(function(){
    $("#error").css("display", "none");
    $("#addquestion").click(function(){
      count = $("#count_question").val();
      count ++;
      $("#count_question").val(count);
      
      var question = '<tr><td>'+count+'</td><td><input type="text" name="question_'+count+'" placeholder="Question" id="question_'+count+'" class="question"></td><td><select name="questionsel_'+count+'" id="questionsel_'+count+'" onchange="question_sel(\'questionsel_'+count+'\')" class="question_sel"><option value="multiple_'+count+'">Multiline Text</option><option value="single_'+count+'">Single Line</option><option value="option_'+count+'">Multiple Choices</option></select></td></tr>';
      question += '<tr id="content_answer_'+count+'"><td></td><td ><textarea name="main_answer_'+count+'" id="main_answer_'+count+'"></textarea></td></tr>';
      $("#question_table").append(question);
          
    });
  });
 
  $("#save").click(function(e){ 
  
  var isValid = true;

    $('input, textarea').each(function() {

        if ($.trim($(this).val()) == '') {

            isValid = false;

            $(this).css({

                "border": "1px solid red",

                "background": "#FFCECE"

            });
            
            $("#error").css("display", "block");

        }

        else {

            $(this).css({

                "border": "",

                "background": ""

            });

        }

    });

    if (isValid == false)
        e.preventDefault();
  });
 
 
 
 function question_sel(val){
    
    selected_value = $("#"+val+" option:selected").val();
    var result_name = selected_value.split('_');
    var value_name = result_name[0];
    var value_count = result_name[1];
        
    if(value_name == 'multiple'){
                var content = '<td></td><td ><textarea name="main_answer_'+value_count+'" id="main_answer_'+value_count+'"></textarea></td><td></td>';
        $('#content_answer_'+value_count).html(content);
    }
    if(value_name == 'single'){
                var content = '<td></td><td><input type="text" name="main_answer_'+value_count+'" id="main_answer_'+value_count+'"></td><td><a style="cursor:pointer" id="'+value_count+'" onclick="add_subquestion('+value_count+')">+ Add Sub Question</a><input type="hidden" name="sub_question_'+value_count+'_count" id="sub_question_'+value_count+'_count" value="0"></td>';
        $('#content_answer_'+value_count).html(content);

    }
    if(value_name == 'option'){
        var content = '';
        
        for(var j=1 ; j<=5 ;j++){
        
                content += '<td><input type="text" name="main_answer_'+value_count+'_'+j+'" id="main_answer_'+value_count+'_'+j+'"></td>';
        }
        
        $('#content_answer_'+value_count).html(content);
    }
 }

function sub_question_sel(val){

    selected_value = $("#"+val+" option:selected").val();
    var result_name = selected_value.split('_');
    var value_name = result_name[0];
    var value_count = result_name[1]; 
    var value_count_inn = result_name[2]; 
   
    if(value_name == 'multiple'){
        var content = '<td></td><td ><textarea name="sub_answer" id="sub_answer"></textarea></td><td></td>';
        $('#content_sub_answer_'+value_count+'_'+value_count_inn).html(content);
    }
    if(value_name == 'single'){
        var content = '<td></td><td><input type="text" name="sub_answer_'+value_count+"_"+value_count_inn+'"   id="sub_answer_'+value_count+"_"+value_count_inn+'"></td><td><a style="cursor:pointer" id="'+val+'_'+value_count_inn+'" onclick="add_subquestion('+value_count+ ')">+ Add Sub Question</a></td>';
        $('#content_sub_answer_'+value_count+'_'+value_count_inn).html(content);
    }
    if(value_name == 'option'){
        var content = '';
        
        for(var j=1 ; j<=5 ;j++){
        
        //content += '<td><input type="text" name="sub_answer_'+value_count+"_"+j+'" id="sub_answer_'+value_count+"_"+j+'"></td><br/>';
        content += '<td><input type="text" name="sub_answer_'+value_count+"_"+value_count_inn+"_"+j+'" id="sub_answer_'+value_count+"_"+value_count_inn+"_"+j+'"></td><br/>';
        }
        
        $('#content_sub_answer_'+value_count+'_'+value_count_inn).html(content);
    }
 }


function add_subquestion(val){

  count = $("#sub_question_"+val+"_count").val(); // "count" is storing Sub Que Count while "val" is Main Que count
    count ++;
    $("#sub_question_"+val+"_count").val(count);
        var sub_question = '<tr><td></td><td><input type="text" name="sub_question_'+val+'_'+count+'" placeholder="Sub Question" id="sub_question_'+val+'_'+count+'" class="sub_question"></td><td><select name="sub_questionsel_'+val+'_'+count+'" id="sub_questionsel_'+val+'_'+count+'" onchange="sub_question_sel(\'sub_questionsel_'+val+'_'+count+'\')" class="question_sel"><option value="multiple_'+val+'_'+count+'">Multiline Text</option><option value="single_'+val+'_'+count+'">Single Line</option><option value="option_'+val+'_'+count+'">Multiple Choices</option></select></td></tr>';
          sub_question += '<tr id="content_sub_answer_'+val+'_'+count+'"><td></td><td ><textarea name="sub_answer_'+val+'_'+count+'" id="sub_answer_'+val+'_'+count+'"></textarea></td><td></td></tr>';
                
          main_que_count = $("#count_question").val();
                    
          if(val == main_que_count) {
            console.log("Value same");
            $('#question_table tr:last').after(sub_question);  // working but commented for a while
          }
          else {
          console.log("Value different");
            var temp = count ;
            temp--;
          
            $('#content_sub_answer_'+val+'_'+temp).after(sub_question);
          
          }
}
</script>
